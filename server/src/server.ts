import * as Express from 'express';
import * as BodyParser from 'body-parser';
import { fetchListings, fetchTicker } from './coinmarket-service';

var app = Express();

app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());

var port = process.env.PORT || 8085;
var router = Express.Router();

router.get('/', (req, res) => {
  res.json({ message: 'knockout is fun' });
});

router.get('/listing', (req, res) => {
  fetchListings().then(r => res.json(r));
});
router.get('/ticker/:id', (req, res) => {
  fetchTicker(req.params.id).then(r => res.json(r));
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
}, router);

app.use('/api', router);

app.listen(port);
console.log(`Server started on port ${port}`);
