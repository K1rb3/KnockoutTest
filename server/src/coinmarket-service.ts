import axios from 'axios';

const API_PATH = 'https://api.coinmarketcap.com/v2';

export function fetchListings(): Promise<any> {
  return axios.get<any>(`${API_PATH}/listings`).then(r => {
    return r.data;
  });
}

export function fetchTicker(id: string): Promise<any> {
  return axios.get<any>(`${API_PATH}/ticker/${id}?`).then(r => {
    return r.data;
  });
}
