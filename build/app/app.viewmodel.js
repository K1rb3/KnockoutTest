"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("util/component");
require("./app.viewmodel.scss");
var AppViewModel = /** @class */ (function () {
    function AppViewModel() {
        this.title = 'Knockout Base!';
    }
    AppViewModel = __decorate([
        component_1.Component({
            name: 'app',
            template: "<h2 data-bind=\"text: title\"></h2>"
        })
    ], AppViewModel);
    return AppViewModel;
}());
exports.AppViewModel = AppViewModel;
//# sourceMappingURL=app.viewmodel.js.map