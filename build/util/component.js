"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var knockout_1 = require("knockout");
exports.Component = function (_a) {
    var _b = _a.name, name = _b === void 0 ? 'bla' : _b, _c = _a.template, template = _c === void 0 ? null : _c;
    return function (viewModel) {
        return knockout_1.components.register(name, {
            viewModel: viewModel,
            template: template
        });
    };
};
//# sourceMappingURL=component.js.map