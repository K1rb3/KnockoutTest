import './app.viewmodel.scss';

export class AppViewModel {
  title = 'Coin Case';
}

export const AppComponent = {
  name: 'app',
  component: {
    viewModel: AppViewModel,
    template: `<navbar params="brand: title"></navbar>
                   <div class="main-content">
                       <home-page></home-page>
                   </div>`
  }
};
