interface NavbarViewModelProps {
  brand: string;
}

class NavbarViewModel implements NavbarViewModelProps {
  brand: string;

  constructor({ brand }: NavbarViewModelProps) {
    this.brand = brand;
  }
}

export const NavBarComponent = {
  name: 'navbar',
  component: {
    viewModel: NavbarViewModel,
    template: `<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <span data-bind="text: brand" class="navbar-brand"></span>
                    </nav>`
  }
};
