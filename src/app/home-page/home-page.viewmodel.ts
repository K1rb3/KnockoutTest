import { observableArray, observable, computed } from 'knockout';
import { getListings, getSpecificTicker } from '@api/api';
import { Listing } from '@models/listing';
import './home-page.viewmodel.scss';
import { Ticker } from '@models/ticker';
import { getCoinImage, ImageSize } from '@util/get-coin-image';

class HomePageViewModel {
  listings = observableArray<Listing>([]);
  activeCoinId = observable<number>();
  activeCoin = observable<Ticker>();
  contentLoading = observable<boolean>(false);
  skip = 0;
  take = 16;
  getTicker: (id: number) => void;
  pagedListings: KnockoutComputed<Listing[]>;

  constructor() {
    getListings(this.updateListingData, this.showError);
    this.getTicker = getSpecificTicker(this.updateActiveCoin, this.showError);
    this.pagedListings = computed(() =>
      this.listings().slice(this.skip, this.take)
    );
  }

  updateListingData = (response: any) => {
    this.listings(response.data.data);
  };

  updateActiveCoin = (response: any) => {
    this.activeCoin(response.data.data);
    this.contentLoading(false);
  };

  checkActive = (data: Ticker) =>
    this.activeCoinId() && this.activeCoinId() === data.id;

  listingSelected = (coin: Listing) => {
    this.activeCoinId(coin.id);
    this.contentLoading(true);
    this.getTicker(coin.id);
  };

  getImage = (coin: Ticker) => ({
    src: getCoinImage(coin.id, ImageSize.ThirtyTwo)
  });

  showError = (error: any) => console.log('error', error);
}

export const HomePageComponent = {
  name: 'home-page',
  component: {
    viewModel: HomePageViewModel,
    template: `<div class="container-fluid">
                      <div class="row">
                           <ul id="coin-list" data-bind="foreach: pagedListings" class="list-group col-2">
                               <li data-bind="click: $parent.listingSelected, css: {active: $parent.checkActive($data)}"
                               class="list-group-item">
                                    <span data-bind="text: name"></span>
                                    <img id="coin-image" data-bind="attr: $parent.getImage($data)">
                               </li>
                           </ul>
                           <div id="main-details" data-bind="if: (activeCoin() && !contentLoading())">
                                <main-details params="selectedCoin: activeCoin"></main-details>
                           </div>
                           <div data-bind="if: contentLoading">
                                Loading...
                           </div>
                       </div>
                   </div>`
  }
};
