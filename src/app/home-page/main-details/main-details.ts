import { Ticker } from '@models/ticker';
import { computed } from 'knockout';
import { getCoinImage, ImageSize } from '@util/get-coin-image';
import './main-details.scss';

interface MainDetailsProps {
  selectedCoin: KnockoutObservable<Ticker>;
}

class MainDetailsViewModel implements MainDetailsProps {
  selectedCoin: KnockoutObservable<Ticker>;
  name: KnockoutComputed<string>;
  imageUrl: KnockoutComputed<Object>;
  rank: KnockoutComputed<string>;

  constructor({ selectedCoin }: MainDetailsProps) {
    console.log('Selected Coin: ', selectedCoin());
    this.selectedCoin = selectedCoin;
    this.name = computed(
      () => `${this.selectedCoin().name} (${this.selectedCoin().symbol})`
    );
    this.imageUrl = computed(() => ({
      src: getCoinImage(this.selectedCoin().id, ImageSize.SixtyFour)
    }));
    this.rank = computed(() => `Rank - ${this.selectedCoin().rank}`);
  }
}

export const MainDetailsComponent = {
  name: 'main-details',
  component: {
    viewModel: MainDetailsViewModel,
    template: `
                    <div id="header">
                        <img data-bind="attr: imageUrl">
                        <h2 data-bind="text: name"></h2>
                        <p data-bind="text: rank"></p>
                    </div>
                   `
  }
};
