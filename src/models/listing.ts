export interface Listing {
  id: number;
  name: string;
  symbol: string;
  website_slug: string;
}
