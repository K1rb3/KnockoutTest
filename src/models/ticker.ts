import { Listing } from '@models/listing';

export interface Ticker extends Listing {
  rank: number;
  circulating_supply: number;
  total_supply: number;
  max_supply: number;
  quotes: Quotes;
  last_updated: number;
}

interface Quotes {
  [key: string]: Quote;
}

interface Quote {
  price: number;
  volume_24h: number;
  market_cap: number;
  percent_change_1h: number;
  percent_change_24h: number;
  percent_change_7d: number;
}
