import axios from 'axios';

const baseUrl = 'http://localhost:8085';

export const getListings = (
  success: (response: any) => any,
  failure: (error: any) => any
) => {
  axios
    .get(`${baseUrl}/listing`)
    .then(success)
    .catch(failure);
};

export const getSpecificTicker = (
  success: (response: any) => any,
  failure: (error: any) => any
) => (id: number) => {
  axios
    .get(`${baseUrl}/ticker/${id}`)
    .then(success)
    .catch(failure);
};
