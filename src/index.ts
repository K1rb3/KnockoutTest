import { applyBindings, options, components, bindingHandlers } from 'knockout';
import './main.scss';
import { AppComponent, AppViewModel } from '@app/app.viewmodel';
import { NavBarComponent } from '@app/navbar/navbar.viewmodel';
import { HomePageComponent } from '@app/home-page/home-page.viewmodel';
import { MainDetailsComponent } from '@app/home-page/main-details/main-details';
import { DumpHandler } from '@util/dump.handler';

const customComponents = [
  NavBarComponent,
  AppComponent,
  HomePageComponent,
  MainDetailsComponent
];

const customBindingHandlers = [DumpHandler];

options.deferUpdates = true;

customBindingHandlers.forEach(el => (bindingHandlers[el.name] = el.handler));
customComponents.forEach(el => components.register(el.name, el.component));

applyBindings(new AppViewModel());
