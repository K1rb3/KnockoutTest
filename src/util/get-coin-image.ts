export enum ImageSize {
  Sixteen = 16,
  ThirtyTwo = 32,
  SixtyFour = 64,
  OneHundredAndTwentyEight = 128
}

export const getCoinImage = (id: number, size: ImageSize) =>
  `https://s2.coinmarketcap.com/static/img/coins/${size}x${size}/${id}.png`;
