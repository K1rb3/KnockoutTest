import { components } from 'knockout';
interface ComponentProps {
  name: string;
  template: string;
}
export const Component = ({ name, template }: ComponentProps) => (
  viewModel: Object
) => {
  components.register(name, {
    viewModel: viewModel,
    template
  });
};
