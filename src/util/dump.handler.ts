export const DumpHandler: { name: string; handler: KnockoutBindingHandler } = {
  name: 'dump',
  handler: {
    update: (el, va) => {
      console.log(el, va());
    }
  }
};
